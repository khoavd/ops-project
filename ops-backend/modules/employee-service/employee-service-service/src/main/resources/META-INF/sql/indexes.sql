create index IX_36A293F9 on EMPLOYEE_Employee (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C981C4BB on EMPLOYEE_Employee (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_CFFD06FF on FOO_Foo (field2);
create index IX_B2FCA947 on FOO_Foo (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_905CD589 on FOO_Foo (uuid_[$COLUMN_LENGTH:75$], groupId);