/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.poc.employee.service.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.poc.employee.service.model.Employee;
import com.poc.employee.service.service.base.EmployeeLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * The implementation of the employee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.poc.employee.service.service.EmployeeLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmployeeLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.poc.employee.service.model.Employee",
	service = AopService.class
)
public class EmployeeLocalServiceImpl extends EmployeeLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.poc.employee.service.service.EmployeeLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.poc.employee.service.service.EmployeeLocalServiceUtil</code>.
	 */

	public Employee updateEmployee(String employeeId,
								   String firstName,
								   String lastName,
								   String email,
								   String description,
								   int gender,
								   ServiceContext serviceContext) throws PortalException {
		Employee employee = null;

		Date now = new Date();

		if (employeeId == null || employeeId.trim().isEmpty()) {
			employeeId = UUID.randomUUID().toString();

			employee = employeePersistence.create(employeeId);

			employee.setCompanyId(serviceContext.getCompanyId());
			employee.setGroupId(serviceContext.getScopeGroupId());
			employee.setCreateDate(new Date());
			employee.setModifiedDate(new Date());

			employee.setEmail(email);
		} else {
			employee = employeePersistence.findByPrimaryKey(employeeId);

			employee.setModifiedDate(now);
		}

		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setEmail(email);
		employee.setGender(gender);
		employee.setDescription(description);

		employeePersistence.update(employee);

		return employee;
	}

	public List<Employee> searchEmployee(int start, int end) {
		return employeePersistence.findAll(start, end);
	}

	public int countEmployee() {
		return employeePersistence.countAll();
	}


}