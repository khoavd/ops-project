package com.poc.employee.service.portlet;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.poc.employee.service.constants.EmployeePortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import com.poc.employee.service.service.EmployeeLocalService;
import com.poc.employee.service.utils.EmployeeConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Khoa VU
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Empoyee",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EmployeePortletKeys.EMPOYEE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class EmployeePortlet extends MVCPortlet {

	public void deleteEmployee(
			ActionRequest actionRequest,
			ActionResponse actionResponse)
			throws Exception {

		String employeeId = ParamUtil.getString(actionRequest, "employeeId");

		System.out.println("*****" + employeeId);

	}

	public void editEmployee(ActionRequest actionRequest,
							 ActionResponse actionResponse)
			throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);

		String employeeId = ParamUtil.getString(actionRequest, EmployeeConstants.EMPLOYEE_ID);
		String firstName = ParamUtil.getString(actionRequest, EmployeeConstants.FIRST_NAME);
		String lastName = ParamUtil.getString(actionRequest, EmployeeConstants.LAST_NAME);
		String email = ParamUtil.getString(actionRequest, EmployeeConstants.EMAIL_ADD);
		String description = ParamUtil.getString(actionRequest, EmployeeConstants.DESCRIPTION);
		int gender = ParamUtil.getInteger(actionRequest, EmployeeConstants.GENDER);

		_emEmployeeLocalService.updateEmployee(employeeId,
				firstName,
				lastName,
				email,
				description,
				gender,
				serviceContext);
	}

	@Reference(unbind = "-")
	protected void setEmployeeLocalService(EmployeeLocalService employeeLocalService) {
		_emEmployeeLocalService = employeeLocalService;
	}
	private EmployeeLocalService _emEmployeeLocalService;
}