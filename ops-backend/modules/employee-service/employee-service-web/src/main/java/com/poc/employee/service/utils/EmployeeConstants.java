package com.poc.employee.service.utils;

/**
 * @author Khoa VU
 */
public class EmployeeConstants {
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL_ADD = "email";
    public static final String GENDER = "gender";
    public static final String DESCRIPTION = "description";
}
