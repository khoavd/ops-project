<%@ page import="com.poc.employee.service.service.EmployeeLocalServiceUtil" %>
<%@ include file="/init.jsp" %>

<%
	PortletURL portletURL = renderResponse.createRenderURL();

	String displayStyle = ParamUtil.getString(request, "displayStyle");

	ViewEmployeeManagementToolbarDisplayContext displayContext =
			new ViewEmployeeManagementToolbarDisplayContext(request, renderRequest, renderResponse, displayStyle);

%>

<%!
	private List<Employee> _buildEmployees() {
		return Arrays.asList(_buildEmployee());
	}

	private Employee _buildEmployee() {
		Employee employee = new EmployeeImpl();

		employee.setEmployeeId(UUID.randomUUID().toString());
		employee.setFirstName("Khoa");
		employee.setLastName("Vu");
		employee.setEmail("khoavd.it@gmail.com");
		employee.setDescription("Thread starvation or clock leap detected (housekeeper delta=10m36s544ms487µs800ns)");
		return employee;
	}
%>

<clay:management-toolbar
		componentId="viewEmployeeToolbar"
		creationMenu="<%=  displayContext.getCreationMenu() %>"
		searchContainerId="roleSearch"
		searchFormName="searchFm"
		selectable="<%= false %>"
		showCreationMenu="<%= true %>"
		showSearch="<%= true %>"
/>

<liferay-ui:search-container
		curParam="cur1"
		deltaConfigurable="<%= false %>"
		emptyResultsMessage="there-are-not-any-employee-now"
		iteratorURL="<%= portletURL %>"
		total="<%= EmployeeLocalServiceUtil.countEmployee() %>"
		id="employee"
>
	<liferay-ui:search-container-results
			results="<%= EmployeeLocalServiceUtil.searchEmployee(searchContainer.getStart(), searchContainer.getEnd()) %>"
	/>

	<liferay-ui:search-container-row
			className="com.poc.employee.service.model.Employee"
			escapedModel="<%= true %>"
			keyProperty="employeeId"
			modelVar="curCategory"
	>
		<liferay-portlet:renderURL varImpl="rowURL">
			<portlet:param name="mvcPath" value="/edit_employee.jsp" />
		</liferay-portlet:renderURL>

		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="firstName"
				value="<%= curCategory.getFirstName() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="lastName"
				value="<%= curCategory.getLastName() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="email"
				value="<%= curCategory.getEmail() %>"
		/>
		<liferay-ui:search-container-column-text
				href="<%= portletURL %>"
				cssClass="table-cell-minw-200"
				name="desc"
				value="<%= curCategory.getDescription() %>"
		/>

		<liferay-ui:search-container-column-jsp
				cssClass="entry-action-column"
				path="/employee_action.jsp"
		/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator
			markupView="lexicon"
			type="more"
	/>
</liferay-ui:search-container>
