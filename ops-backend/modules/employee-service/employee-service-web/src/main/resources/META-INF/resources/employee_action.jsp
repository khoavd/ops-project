<%@ include file="/init.jsp" %>

<%

String redirect = currentURL;

ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

Employee employee = (Employee)row.getObject();

String employeeId = employee.getEmployeeId();
%>

<liferay-ui:icon-menu
	direction="left-side"
	icon="<%= StringPool.BLANK %>"
	markupView="lexicon"
	message="<%= StringPool.BLANK %>"
	showWhenSingleIcon="<%= true %>"
>

		<portlet:renderURL var="editEmpoloyeeURL">
			<portlet:param name="mvcPath" value="/edit_employee.jsp" />
			<portlet:param name="redirect" value="<%= currentURL %>" />
			<portlet:param name="employeeId" value="<%= employeeId %>" />
		</portlet:renderURL>

		<liferay-ui:icon
			message="edit"
			url="<%= editEmpoloyeeURL %>"
		/>

		<portlet:actionURL name="deleteEmployee" var="deleteEmployeeURL">
			<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
			<portlet:param name="redirect" value="<%= redirect %>" />
			<portlet:param name="employeeId" value="<%= employeeId %>" />
		</portlet:actionURL>

		<liferay-ui:icon-delete
			url="<%= deleteEmployeeURL %>"
		/>

</liferay-ui:icon-menu>