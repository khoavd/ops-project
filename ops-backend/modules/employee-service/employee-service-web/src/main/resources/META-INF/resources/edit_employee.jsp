<%@ page import="com.poc.employee.service.service.EmployeeServiceUtil" %>
<%@ page import="com.poc.employee.service.service.EmployeeLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ include file="/init.jsp" %>

<%
    String redirect = ParamUtil.getString(request, "redirect");

    String backURL = ParamUtil.getString(request, "backURL", redirect);

    String employeeId = ParamUtil.getString(request, "employeeId");

    Employee employee = EmployeeLocalServiceUtil.fetchEmployee(employeeId);

    portletDisplay.setShowBackIcon(true);
    portletDisplay.setURLBack(backURL);

    renderResponse.setTitle((employee == null) ? LanguageUtil.get(request, "new-employee")
            : employee.getFirstName());
%>

<portlet:actionURL name="editEmployee" var="editEmployeeURL">
    <portlet:param name="mvcPath" value="/edit_employee.jsp" />
    <portlet:param name="backURL" value="<%= backURL %>" />
</portlet:actionURL>

<portlet:renderURL var="editEmployeeRenderURL">
    <portlet:param name="mvcPath" value="/edit_employee.jsp" />
    <portlet:param name="backURL" value="<%= backURL %>" />
    <portlet:param name="employeeId" value="<%= employeeId %>" />
</portlet:renderURL>

<aui:form action="<%= editEmployeeURL %>" cssClass="container-fluid container-fluid-max-xl container-form-view" method="post" name="fm">
    <aui:input name="redirect" type="hidden" value="<%= editEmployeeRenderURL %>" />
    <aui:input name="employeeId" type="hidden" value="<%= employeeId %>" />

    <aui:model-context bean="<%= employee %>" model="<%= Employee.class %>" />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:select name="gender">
                <aui:option label="male" value="1" />
                <aui:option label="female" value="2" />
                <aui:option label="unknow" value="3" />
            </aui:select>

            <aui:input name="firstName" >
                <aui:validator name="required" />
            </aui:input>
            <aui:input name="lastName" >
                <aui:validator name="required" />
            </aui:input>
            <aui:input name="email" >
                <aui:validator name="required" />
            </aui:input>
            <aui:input name="description" />

            <aui:button-row>
                <aui:button type="submit" />
                <aui:button href="<%= backURL %>" type="cancel" />
            </aui:button-row>
        </aui:fieldset>
    </aui:fieldset-group>
</aui:form>
