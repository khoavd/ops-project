package com.demo.rest.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Employee
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-01-31T09:40:57.219+07:00")

public class ErrorMsg {

  @JsonProperty("message")
  private String message = null;

  @JsonProperty("code")
  private int code;

  @JsonProperty("desc")
  private String desc = null;

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }
}

