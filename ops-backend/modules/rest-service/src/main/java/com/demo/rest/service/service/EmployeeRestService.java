package com.demo.rest.service.service;

import com.demo.rest.service.mapper.EmployeeMapper;
import com.demo.rest.service.model.EmployeeRequest;
import com.demo.rest.service.model.EmployeeRes;
import com.demo.rest.service.model.Employees;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.poc.employee.service.model.Employee;
import com.poc.employee.service.service.EmployeeLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.ws.rs.ext.Provider;
import java.util.List;

/**
 * @author Khoa VU
 */

@Component(immediate = true, service = EmployeeRestService.class)
@Provider
public class EmployeeRestService {

    public EmployeeRes updateEmployee(EmployeeRequest empReq) throws PortalException {
        Employee employee = _emEmployeeLocalService.updateEmployee(
                empReq.getEmployeeId(),
                empReq.getFirstName(),
                empReq.getLastName(),
                empReq.getEmailId(),
                empReq.getDesc(),
                empReq.getGender(),
                new ServiceContext());

        return _mapper.mapEmployeeResFromEmployee(employee);
    }

    public Employees getAllEmployees() throws PortalException {
        List<Employee> employeeLs = _emEmployeeLocalService.getEmployees(QueryUtil.ALL_POS, QueryUtil.ALL_POS);

        return _mapper.mapEmployeesFromEmployees(employeeLs);
    }


    @Reference(unbind = "-")
    protected void setEmployeeMapper(EmployeeMapper employeeMapper) {
        _mapper = employeeMapper;
    }

    private EmployeeMapper _mapper;


    @Reference(unbind = "-")
    protected void setEmployeeLocalService(EmployeeLocalService employeeLocalService) {
        _emEmployeeLocalService = employeeLocalService;
    }

    private EmployeeLocalService _emEmployeeLocalService;
}
