package com.demo.rest.service.mapper;

import com.demo.rest.service.model.EmployeeRes;
import com.demo.rest.service.model.Employees;
import com.demo.rest.service.service.EmployeeRestService;
import com.poc.employee.service.model.Employee;
import org.osgi.service.component.annotations.Component;

import javax.ws.rs.ext.Provider;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Khoa VU
 */
@Component(immediate = true, service = EmployeeMapper.class)
@Provider
public class EmployeeMapper {
    public EmployeeRes mapEmployeeResFromEmployee(Employee from) {
        EmployeeRes to = new EmployeeRes();

        to.setId(from.getEmployeeId());
        to.setFirstName(from.getFirstName());
        to.setLastName(from.getLastName());
        to.setEmailId(from.getEmail());
        to.setGender(from.getGender());
        to.setDesc(from.getDescription());

        return to;
    }

    public Employees mapEmployeesFromEmployees(List<Employee> from) {
        return from.stream()
                .map(this::mapEmployeeResFromEmployee)
                .collect(Collectors.toCollection(Employees::new));
    }
}
