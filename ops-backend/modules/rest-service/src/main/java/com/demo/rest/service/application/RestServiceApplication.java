package com.demo.rest.service.application;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.demo.rest.service.model.EmployeeRequest;
import com.demo.rest.service.model.EmployeeRes;
import com.demo.rest.service.model.Employees;
import com.demo.rest.service.model.ErrorMsg;
import com.demo.rest.service.service.EmployeeRestService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

/**
 * @author Khoa VU
 */
@Component(
	property = {
		JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/employees",
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=Greetings.Rest"
	},
	service = Application.class
)
public class RestServiceApplication extends Application {

	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}


	@POST
	@Path("/status2")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
	public Response addEmployee(@BeanParam EmployeeRequest input) {
		try {
			EmployeeRes res = _employeeRestService.updateEmployee(input);

			return Response.status(200).entity(res).build();

		} catch (Exception e) {
			ErrorMsg error = new ErrorMsg();

			error.setMessage("Update Employee Error");
			error.setCode(500);
			error.setDesc(e.getMessage());

			return Response.status(500).entity(error).build();
		}
	}

	@GET
	@Path("/status2")
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Response getEmployees() {
		try {
			Employees res = _employeeRestService.getAllEmployees();

			return Response.status(200).entity(res).build();

		} catch (Exception e) {
			ErrorMsg error = new ErrorMsg();

			error.setMessage("Get Employee Error");
			error.setCode(500);
			error.setDesc(e.getMessage());

			return Response.status(500).entity(error).build();
		}
	}

	@GET
	@Path("/status")
	@Produces("text/plain")
	public String working() {
		return "It works!";
	}

	@Reference(unbind = "-")
	protected void setEmployeeRestService(EmployeeRestService employeeRestService) {
		_employeeRestService = employeeRestService;
	}



	private EmployeeRestService _employeeRestService;
}