package com.demo.rest.service.model;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * @author Khoa VU
 */
@XmlRegistry
public class ObjectFactory {
    public ObjectFactory() {
    }

    public EmployeeRes createEmployeeRes() {
        return new EmployeeRes();
    }

    public EmployeeRequest createEmployeeRequest() {
        return new EmployeeRequest();
    }

    public Employees createEmployees() {
        return new Employees();
    }

    public ErrorMsg createErrorMsg() {
        return new ErrorMsg();
    }
}
